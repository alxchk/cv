<xsl:stylesheet
    version="1.0"
    xmlns:cv="http://alxchk.me/cv"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output
      method="html"
      encoding="utf-8"
      omit-xml-declaration="yes"
      doctype-system="html"
      indent="yes"
      media-type="string"/>

  <xsl:template match="/">
    <html lang="en">
      <head>
	<title>
	  <xsl:value-of select="//cv:person/cv:firstname"/><xsl:text> </xsl:text>
	  <xsl:value-of select="//cv:person/cv:familyname"/><xsl:text> (</xsl:text>
	  <xsl:value-of select="//cv:person/cv:speciality"/><xsl:text>)</xsl:text>
	</title>

	<xsl:variable name="firstname" select="/cv:CV/cv:person/cv:firstname" />
	<xsl:variable name="familyname" select="/cv:CV/cv:person/cv:familyname" />

	<meta name="viewport" content="width=device-width,initial-scale=1"/>
	<meta name="description" content="CV"/>
	<meta name="apple-mobile-web-app-capable" content="no"/>
	<meta name="keywords" content="CV"/>
	<meta name="author" content="{$firstname} {$familyname}"/>

	<meta charset="UTF-8"/>
	<link rel="stylesheet" href="cv-html.css" type="text/css" />
	<link rel="stylesheet" href="fonts/barlow.css" type="text/css"/>
	<link rel="stylesheet" href="fonts/font-awesome.min.css" type="text/css" />

      </head>

      <body id="top">
	<div id="cv">
	  <div id="person">
	    <xsl:apply-templates select="//cv:person"/>
	    <xsl:apply-templates select="//cv:contacts"/>
	  </div> <!-- mainDetails -->

	  <div id="cvbody">
	    <section class="summary">
	      <div class="section"></div>
	      <div class="title">
		<h1 class="sectitle">Summary</h1>
	      </div>
	      <div class="clear"></div>

	      <div id="summary">
		<xsl:apply-templates select="//cv:summary/cv:section"/>
	      </div>
	      <div class="clear"></div>

	    </section>

	    <section id="employments">
	      <div class="section"></div>
	      <div class="title">
		<h1 class="sectitle">Employment</h1>
	      </div>
	      <div class="clear"></div>

	      <div class="new">
		<xsl:apply-templates select="//cv:employments/cv:item[not(@old = 'true')]"/>
	      </div>
	      <div class="old">
		<xsl:apply-templates select="//cv:employments/cv:item[@old = 'true']"/>
	      </div>
	      <div class="clear"></div>
	    </section>

	    <section id="education">
	      <div class="section"></div>
	      <div class="title">
		<h1 class="sectitle">Education</h1>
	      </div>
	      <div class="clear"></div>

	      <div class="new">
		<xsl:apply-templates select="//cv:education/cv:item[not(@old = 'true')]"/>
	      </div>
	      <div class="old">
		<xsl:apply-templates select="//cv:education/cv:item[@old = 'true']"/>
	      </div>
	      <div class="clear"></div>

	    </section>

	  </div> <!-- mainArea -->
	</div><!-- cv -->
      </body>

      <xsl:apply-templates/>
    </html>
  </xsl:template>

  <xsl:template match="//cv:person">
    <div id="avatar">
      <img src="{cv:avatar}" alt="{cv:firstname} {cv:familyname}" />
    </div>

    <div id="name">
      <h1><xsl:value-of select="cv:firstname"/><xsl:text> </xsl:text><xsl:value-of select="cv:familyname"/></h1>
      <h2><xsl:value-of select="cv:speciality"/></h2>
    </div>
  </xsl:template>

  <xsl:template match="//cv:contacts">
    <div id="contacts">
      <ul>
	<xsl:apply-templates select="//cv:contacts/cv:item"/>
      </ul>
    </div>
    <div class="clear"></div>

  </xsl:template>

  <xsl:template match="//cv:contacts/cv:item">
    <li class="contact fa-{@class}">
      <xsl:choose>
	<xsl:when test="./@href">
	  <a rel="noopener" href="{@href}"><xsl:value-of select="."/></a>
	</xsl:when>
	<xsl:otherwise>
	  <a rel="noopener" href="{@schema}:{.}" target="_blank"><xsl:value-of select="."/></a>
	</xsl:otherwise>
      </xsl:choose>
    </li>
  </xsl:template>

  <xsl:template match="//cv:summary/cv:section">
    <article>
      <div class="sectitle">
	<h1><xsl:value-of select="@name"/></h1>
	<div class="subsection"></div>
      </div>

      <div class="secbody">
	<p>
	  <xsl:for-each select="cv:item">
	    <xsl:value-of select="." /><xsl:text>. &#8203;</xsl:text>
	  </xsl:for-each>
	</p>
      </div>
      <div class="clear"></div>
    </article>
  </xsl:template>

  <xsl:template match="//cv:employments/cv:item">
    <xsl:variable name="employer" select="//cv:employers/cv:employer[cv:id = current()/@employer]" />
    <xsl:variable name="new" select="not(preceding-sibling::cv:item/@employer = @employer)" />

    <div class="employment">
      <div class="sectitle">
	<h1><xsl:value-of select="@from"/><xsl:text>-&#8203;</xsl:text><xsl:value-of select="@to"/></h1>
	<xsl:if test="$new">
	  <img class="logo" src="{$employer/cv:logo}"/>
	</xsl:if>
      </div>
      <div class="secbody">
	<article>
	  <div class="secheader">
	    <h2>
	      <xsl:value-of select="cv:grade"/>
	      <xsl:choose>
		<xsl:when test="$new">
		  <a class="corp" rel="noopener" href="{$employer/cv:url}"><xsl:value-of select="$employer/cv:name"/></a>
		</xsl:when>
		<xsl:otherwise>
		  <a class="corp" rel="noopener" href="{$employer/cv:url}">same</a>
		</xsl:otherwise>
	      </xsl:choose>
	    </h2>
	    <xsl:if test="cv:summary">
	      <h3><xsl:value-of select="cv:summary"/></h3>
	    </xsl:if>
	  </div>
	  <div class="clear"></div>
	  <xsl:if test="cv:groups">
	    <div class="worktasks">
	      <ul>
		<xsl:apply-templates select="./cv:groups/cv:item"/>
	      </ul>
	    </div>
	  </xsl:if>
	</article>
      </div>
      <div class="clear"></div>
      <div class="subsection"></div>
    </div>
  </xsl:template>

  <xsl:template match="//cv:education/cv:item">
    <xsl:variable name="school" select="//cv:schools/cv:school[cv:id = current()/@school]" />
    <xsl:variable name="new" select="not(preceding-sibling::cv:item/@school = @school)" />
    <xsl:variable name="speciality" select="//cv:specialities/cv:speciality[cv:id = current()]/cv:text" />

    <div class="employment">
      <div class="sectitle">
	<h1><xsl:value-of select="@from"/><xsl:text>-&#8203;</xsl:text><xsl:value-of select="@to"/></h1>
      </div>
      <div class="secbody">
	<article>
	  <div class="secheader">
	    <h2>
	      <xsl:value-of select="@grade"/>
	      <xsl:choose>
		<xsl:when test="$new">
		  <a class="corp" rel="noopener" href="{$school/cv:url}"><xsl:value-of select="$school/cv:name"/></a>
		</xsl:when>
		<xsl:otherwise>
		  <a class="corp" rel="noopener" href="{$school/cv:url}">same</a>
		</xsl:otherwise>
	      </xsl:choose>
	    </h2>
	  </div>
	  <div class="clear"></div>
	  <div class="worktasks">
	    <span class="keyword"><xsl:value-of select="."/>. &#8203;</span>
	    <span cass="task">
	      <xsl:value-of select="$speciality"/>. &#8203;
	    </span>
	  </div>
	</article>
      </div>
      <div class="clear"></div>
      <div class="subsection"></div>
    </div>
  </xsl:template>

  <xsl:template match="//cv:employments/cv:item/cv:groups/cv:item">
    <li>
      <span class="keyword"><xsl:value-of select="@name"/><xsl:text>. &#8203;</xsl:text></span>
      <xsl:if test="cv:tasks/cv:item">
	<xsl:for-each select="cv:tasks/cv:item">
	  <span class="task"><xsl:value-of select="text()" />
	  <xsl:if test="cv:personal"> (<a class="icon fa-users"/>)</xsl:if>
	  <xsl:text>. &#8203;</xsl:text></span>
	  <xsl:if test="cv:personal">
	    <xsl:for-each select="cv:personal/cv:item">
	      <span class="task">
		<xsl:value-of select="text()" />
		<!-- <xsl:if test="position() = last()"> (<a class="icon user"/>)</xsl:if> -->
		<xsl:text>. &#8203;</xsl:text>
	      </span>&#8203;
	    </xsl:for-each>
	  </xsl:if>
	</xsl:for-each>
	<p/>
      </xsl:if>
      <xsl:if test="cv:skills/cv:item">
	<a class="icon fa-desktop"/>
	<xsl:for-each select="cv:skills/cv:item">
	  <span class="skill"><xsl:value-of select="." />
	  <xsl:if test="position() != last()"><xml:text>/&#8203;</xml:text></xsl:if>
	  </span>
	</xsl:for-each>
	<xsl:text>. &#8203;</xsl:text>
      </xsl:if>
      <xsl:if test="cv:systems/cv:item">
	<xsl:for-each select="cv:systems/cv:item">
	  <span class="system"><xsl:value-of select="." />
	  <xsl:if test="position() != last()"><xml:text>/&#8203;</xml:text></xsl:if>
	  </span>
	</xsl:for-each>
	<xsl:text>. &#8203;</xsl:text>
	<xsl:if test="cv:tools/cv:item">
	  <br/>
	</xsl:if>
      </xsl:if>
      <xsl:if test="cv:tools/cv:item">
	<a class="icon fa-wrench"/>
	<xsl:for-each select="cv:tools/cv:item">
	  <span class="tool"><xsl:value-of select="." />
	  <xsl:if test="position() != last()"><xml:text>/&#8203;</xml:text></xsl:if>
	  </span>
	</xsl:for-each>
	<xsl:text>. &#8203;</xsl:text>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template match="*">
  </xsl:template>
</xsl:stylesheet>
